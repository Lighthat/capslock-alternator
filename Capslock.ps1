$continue = $true
echo "Toggle with F12";
while(1)
{
sleep 0.8
    while($continue)
    {

        if ([console]::KeyAvailable)
        {
            echo "Toggled off";
            $x = [System.Console]::ReadKey() 

            switch ( $x.key)
            {
                F12 { $continue = $false }
            }
        } 
        else
        {
            $wsh = New-Object -ComObject WScript.Shell
            $wsh.SendKeys('{CAPSLOCK}')
            sleep 0.5
            [System.Runtime.Interopservices.Marshal]::ReleaseComObject($wsh)| out-null
            Remove-Variable wsh
        }    
    }
    if ([console]::KeyAvailable)
        {
            echo "Toggle with F12";
            $x = [System.Console]::ReadKey() 

            switch ( $x.key)
            {
                F12 { $continue = $true;echo "Toggled on"; }
            }
        } 
}